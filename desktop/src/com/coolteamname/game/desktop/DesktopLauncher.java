package com.coolteamname.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.coolteamname.game.Application;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Track Hunter / MVP / COOL TEAM NAME";
		config.height = 648;
		config.width = 800;
		new LwjglApplication(new Application(), config);
	}
}
