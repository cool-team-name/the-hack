package com.coolteamname.game.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.*;
import com.coolteamname.game.managers.BulletManager;
import com.coolteamname.game.managers.DoorManager;
import com.coolteamname.game.managers.StatManager;

/**
 * Class to represent the player
 */
public class Player {

    private Sprite sprite;
    private Sprite healthBar;
    private float speed = 250.0f;
    private int maxHealth = 6;
    private int health = 6;
    private float shootDelay = 0.35f;
    private float shootTimer = 10;
    private Rectangle playerRectangle;
    private StatManager statManager;
    private float deltaX = 0;
    private float deltaY = 0;
    private boolean invincible;
    private boolean reducedShootTimer;

    /**
     * Constructor for the player entity
     *
     * @param xPos        initial x position
     * @param yPos        initial y position
     * @param texture     player texture
     * @param barTexture  healthbar texture
     * @param statManager reference to the stat manager
     */
    public Player(float xPos, float yPos, Texture texture, Texture barTexture, StatManager statManager) {
        this.sprite = new Sprite(texture);
        this.sprite.setPosition(xPos, yPos);
        this.healthBar = new Sprite(barTexture);
        this.healthBar.setPosition(xPos - 110, yPos - 140);
        this.playerRectangle = new Rectangle();
        this.playerRectangle.setX(sprite.getX());
        this.playerRectangle.setY(sprite.getY());
        this.playerRectangle.setHeight(sprite.getHeight());
        this.playerRectangle.setWidth(sprite.getWidth());
        this.statManager = statManager;
        this.invincible = false;
    }

    /**
     * Getter for players x position
     *
     * @return players x position
     */
    public float getX() {
        return sprite.getX();
    }

    /**
     * Getter for players y position
     *
     * @return players y position
     */
    public float getY() {
        return sprite.getY();
    }

    /**
     * Getter for player rectangle
     *
     * @return player collision rectangle
     */
    public Rectangle getPlayerRectangle() {
        return this.playerRectangle;
    }

    /**
     * Getter for player death status
     *
     * @return is player alive or dead
     */
    public boolean isDead() {
        return health <= 0;
    }

    /**
     * Self explanatory method name, clear naming of methods is valuable and important in any development team.
     *
     * @param invincible sets whether the player is invincible or not
     */
    public void becomeSingularMostPowerfulBeingInTheEntireUniverse(boolean invincible) {
        this.invincible = invincible;
    }

    /**
     * Setter for reduced shoot timer powerup
     *
     * @param reducedShootTimer sets wheter reduced shoot timer is enabled
     */
    public void setReducedShootTimer(boolean reducedShootTimer) {
        this.reducedShootTimer = reducedShootTimer;
    }

    /**
     * Reset player position and health
     *
     * @param playerPosX new player x position
     * @param playerPosY new player y position
     */
    public void reset(float playerPosX, float playerPosY) {
        this.health = this.maxHealth;
        this.setPos(playerPosX, playerPosY);
    }

    /**
     * Helper function to update player rectangle and sprite position
     *
     * @param playerPosX new player x position
     * @param playerPosY new player y position
     */
    public void setPos(float playerPosX, float playerPosY) {
        this.sprite.setPosition(playerPosX, playerPosY);
        this.playerRectangle.setX(sprite.getX());
        this.playerRectangle.setY(sprite.getY());
        this.playerRectangle.setHeight(sprite.getHeight());
        this.playerRectangle.setWidth(sprite.getWidth());
    }

    /**
     * Helper function to check collision between two rectangles and adjust player movement accordingly
     *
     * @param rectangle           rectangle to check against
     * @param potentialPlayerRect the rectangle the player would occupy without collision detection
     */
    private void checkCollision(Rectangle rectangle, Rectangle potentialPlayerRect) {
        if (Intersector.overlaps(rectangle, potentialPlayerRect)) {
            if (potentialPlayerRect.getX() + potentialPlayerRect.getWidth() > rectangle.getX() &&
                    potentialPlayerRect.getX() + potentialPlayerRect.getWidth() < rectangle.getX() + rectangle.getWidth())
                if (deltaX > 0)
                    deltaX = 0;
            if (potentialPlayerRect.getX() < rectangle.getX() + rectangle.getWidth() &&
                    potentialPlayerRect.getX() > rectangle.getX())
                if (deltaX < 0)
                    deltaX = 0;

            if (potentialPlayerRect.getY() + potentialPlayerRect.getHeight() > rectangle.getY() &&
                    potentialPlayerRect.getY() + potentialPlayerRect.getHeight() < rectangle.getY() + rectangle.getHeight())
                if (deltaY > 0)
                    deltaY = 0;
            if (potentialPlayerRect.getY() < rectangle.getY() + rectangle.getHeight() &&
                    potentialPlayerRect.getY() > rectangle.getY())
                if (deltaY < 0)
                    deltaY = 0;
        }
    }

    /**
     * Update the player each frame to handle collision, movement, shooting etc
     *
     * @param camera           reference to the camera
     * @param bulletManager    reference to the bullet manager
     * @param collisionObjects collision objects to check against for player movement
     * @param doorManager      reference to the door manager
     */
    public void update(Camera camera, BulletManager bulletManager, MapObjects collisionObjects, DoorManager doorManager) {

        if (this.reducedShootTimer) {
            this.shootDelay = 0.2f;
        }

        this.playerRectangle.setX(sprite.getX());
        this.playerRectangle.setY(sprite.getY());

        // Update fire rate timer
        shootTimer += Gdx.graphics.getDeltaTime();

        // Rotate Player
        Vector3 mousePos = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
        float angle = MathUtils.radiansToDegrees *
                MathUtils.atan2(mousePos.y - sprite.getY(), mousePos.x - sprite.getX());
        sprite.setRotation(angle);

        // Get bullet direction
        Vector3 direction = mousePos.sub(sprite.getX(), sprite.getY(), 0).nor();

        // Player Inputs
        deltaX = 0;
        deltaY = 0;

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A))
            deltaX = -1 * speed * Gdx.graphics.getDeltaTime();
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D))
            deltaX = speed * Gdx.graphics.getDeltaTime();
        if (Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.W))
            deltaY = speed * Gdx.graphics.getDeltaTime();
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.S))
            deltaY = -1 * speed * Gdx.graphics.getDeltaTime();
        if (Gdx.input.isTouched()) {
            // Limit fire rate
            if (!reducedShootTimer && shootTimer >= bulletManager.getShootDelay()) {
                shootTimer = 0;
                statManager.playerShoot();
                bulletManager.shootBullet(sprite.getX(), sprite.getY(), angle, new Vector2(direction.x, direction.y), true);
            } else if (reducedShootTimer && shootTimer >= shootDelay) {
                shootTimer = 0;
                statManager.playerShoot();
                bulletManager.shootBullet(sprite.getX(), sprite.getY(), angle, new Vector2(direction.x, direction.y), true);
            }
        }

        Rectangle potentialPlayerRect = playerRectangle;

        potentialPlayerRect.setX(sprite.getX() + deltaX);
        potentialPlayerRect.setY(sprite.getY() + deltaY);

        // Collision Detection
        for (RectangleMapObject rectangleMapObject : collisionObjects.getByType(RectangleMapObject.class)) {
            Rectangle rectangle = rectangleMapObject.getRectangle();

            checkCollision(rectangle, potentialPlayerRect);
        }

        for (Door door : doorManager.getDoorList()) {
            Rectangle rectangle = door.getCollisionRectangle();
            if (!door.isOpened())
                checkCollision(rectangle, potentialPlayerRect);
        }

        // Update player position
        sprite.setX(sprite.getX() + deltaX);
        sprite.setY(sprite.getY() + deltaY);

        healthBar.setPosition(sprite.getX() - 110, sprite.getY() - 140);
        healthBar.setScale(0.025f * health, 0.03f);

        // Check if hit by bullet
        for (Bullet bullet : bulletManager.getBulletList()) {
            if (!bullet.isPlayerOrigin() && bullet.getCollisionRectangle().overlaps(playerRectangle)) {
                statManager.playerHit();
                bullet.setFinished();
                if (!this.invincible)
                    health--;
            }
        }
    }

    /**
     * Render method to render the player sprite and a healthbar
     */
    public void render(SpriteBatch spriteBatch) {
        sprite.draw(spriteBatch);
        healthBar.draw(spriteBatch);
    }
}
