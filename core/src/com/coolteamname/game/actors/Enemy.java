package com.coolteamname.game.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.coolteamname.game.managers.BulletManager;
import com.coolteamname.game.managers.StatManager;

/**
 * Class to represent each enemy in the game and cause them to shoot at a player.
 */
public class Enemy extends Actor {

    private Sprite healthBar;
    private float shootDelay = 0.8f;
    private float shootTimer = 10;
    private boolean dead = false;
    private int health = 3;
    private StatManager statManager;

    /**
     * Constructor for each Enemy instance.
     *
     * @param xPos        initial x position
     * @param yPos        initial y position
     * @param texture     texture of enemies
     * @param barTexture  texture of health bars
     * @param statManager reference to the statmanager
     */
    public Enemy(float xPos, float yPos, Texture texture, Texture barTexture, StatManager statManager) {
        super(texture, xPos, yPos);
        this.healthBar = new Sprite(barTexture);
        this.healthBar.setPosition(xPos - 110, yPos - 140);
        this.statManager = statManager;
    }

    /**
     * Getter to check if the enemy is alive or dead
     *
     * @return whether the enemy is alive or dead
     */
    public boolean isDead() {
        return dead;
    }

    /**
     * Updates the enemy each frame causing it to shoot at a player and turn to face the player
     *
     * @param player        reference to the player
     * @param bulletManager reference to the bulletmanager
     */
    public void update(Player player, BulletManager bulletManager) {
        // Update fire rate timer
        shootTimer += Gdx.graphics.getDeltaTime();

        // Rotate Enemy towards Player
        Vector3 playerPos = new Vector3(player.getX(), player.getY(), 0);
        float angle = MathUtils.radiansToDegrees *
                MathUtils.atan2(playerPos.y - sprite.getY(), playerPos.x - sprite.getX());
        sprite.setRotation(angle);

        Vector3 direction = playerPos.sub(sprite.getX(), sprite.getY(), 0).nor();

        // If player is nearby then shoot them
        if (player.getX() > this.getX() - 550 && player.getX() < this.getX() + 550 &&
                player.getY() > this.getY() - 500 && player.getY() < this.getY() + 500) {
            // Limit fire rate
            if (shootTimer >= shootDelay) {
                shootTimer = 0;
                statManager.enemyShoot();
                bulletManager.enemyShootBullet(this.getX(), this.getY(), angle, new Vector2(direction.x, direction.y), false);
            }
        }

        collisionRectangle.setX(sprite.getX());
        collisionRectangle.setY(sprite.getY());

        healthBar.setScale(0.05f * health, 0.03f);

        // Check if hit by bullet
        for (Bullet bullet : bulletManager.getBulletList()) {
            if (bullet.isPlayerOrigin() && bullet.getCollisionRectangle().overlaps(collisionRectangle)) {
                bullet.setFinished();
                statManager.enemyHit();
                health--;
                if (health <= 0) {
                    dead = true;
                }
            }
        }
    }

    /**
     * Custom render function that also renders enemy health bar
     *
     * @param spriteBatch spritebatch to use for rendering
     */
    @Override
    public void render(SpriteBatch spriteBatch) {
        super.render(spriteBatch);
        healthBar.draw(spriteBatch);
    }
}
