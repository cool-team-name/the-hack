package com.coolteamname.game.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.coolteamname.game.managers.BulletManager;
import com.coolteamname.game.managers.PowerupManager;

/**
 * Class to represent and manage powerup conditions
 */
public class PowerupPickup extends Actor {

    private boolean collected;
    private boolean active;
    private float duration;
    private float timer;
    private boolean invincible;
    private boolean reducedShootTimer;
    private boolean multiShot;
    private PowerupManager powerupManager;

    /**
     * Constructor for each PowerupPickup
     *
     * @param texture           texture for powerups
     * @param xPos              initial x position
     * @param yPos              initial y position
     * @param duration          duration of the powerup
     * @param invincible        is player invincible
     * @param reducedShootTimer does player have reduced shoot timer
     * @param multiShot         does player have multishot
     * @param powerupManager    reference to the powerupManager
     */
    public PowerupPickup(Texture texture, float xPos, float yPos, int duration, boolean invincible, boolean reducedShootTimer, boolean multiShot, PowerupManager powerupManager) {
        super(texture, xPos, yPos);
        this.collected = false;
        this.active = false;
        this.duration = duration;
        this.timer = 0;
        this.invincible = invincible;
        this.reducedShootTimer = reducedShootTimer;
        this.multiShot = multiShot;
        this.powerupManager = powerupManager;
    }

    /**
     * Sets all powerups to new values
     *
     * @param player            reference to the player
     * @param bulletManager     reference to the bullet manager
     * @param invincible        is player invincible
     * @param reducedShootTimer does player have reduced shoot timer
     * @param multiShot         does player have multishot
     */
    private void togglePowerups(Player player, BulletManager bulletManager, boolean invincible, boolean reducedShootTimer, boolean multiShot) {
        player.becomeSingularMostPowerfulBeingInTheEntireUniverse(invincible);
        player.setReducedShootTimer(reducedShootTimer);
        bulletManager.setMultiShot(multiShot);
    }

    /**
     * Update the pickup each frame and check for pickup and activation
     *
     * @param player        reference to the player
     * @param bulletManager reference to the bullet manager
     */
    public void update(Player player, BulletManager bulletManager) {
        if (!this.collected && player.getPlayerRectangle().overlaps(this.collisionRectangle)) {
            this.powerupManager.enableTitle();
            this.collected = true;
            this.active = true;
            togglePowerups(player, bulletManager, this.invincible, this.reducedShootTimer, this.multiShot);
        }
        if (this.timer >= this.duration) {
            this.active = false;
        }
        if (active) {
            this.timer += Gdx.graphics.getDeltaTime();
        } else {
            togglePowerups(player, bulletManager, false, false, false);
        }
    }

    /**
     * Custom render function to conditionally render the powerup
     *
     * @param spriteBatch spritebatch to use for rendering
     */
    @Override
    public void render(SpriteBatch spriteBatch) {
        if (!this.collected)
            super.render(spriteBatch);
    }

    /**
     * Render powerup titles
     *
     * @param spriteBatch gui spritebatch
     */
    public void renderTitle(SpriteBatch spriteBatch) {
        if (active) {
            String text = "";
            if (invincible) {
                text += "++ Invincible\n";
            }
            if (reducedShootTimer) {
                text += "++ Faster Shooting\n";
            }
            if (multiShot) {
                text += "++ Multishot\n";
            }
            font.draw(spriteBatch, text, -10, -40);
        }
    }
}
