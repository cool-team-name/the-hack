package com.coolteamname.game.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.coolteamname.game.managers.DoorManager;

/**
 * Class that represents each keycard, it's status and a link to the door it opens
 */
public class Keycard extends Actor {

    private String doorId;
    private boolean collected;
    private boolean near;

    /**
     * Constructor for each Keycard instance
     *
     * @param xPos    initial x position
     * @param yPos    initial y position
     * @param texture keycard texturee
     * @param doorId  id of the door the keycard unlocks
     */
    public Keycard(float xPos, float yPos, Texture texture, String doorId) {
        super(texture, xPos, yPos);
        this.doorId = doorId;
        this.collected = false;
        this.near = false;
    }

    public String getDoorId() {
        return doorId;
    }

    public boolean isCollected() {
        return collected;
    }

    /**
     * Updates the keycard each frame to alter state appropriately
     *
     * @param player      reference to the player
     * @param doorManager reference to the door manager
     */
    public void update(Player player, DoorManager doorManager) {
        if (player.getPlayerRectangle().overlaps(this.collisionRectangle)) {
            this.collected = true;
            doorManager.setUnlocked(doorId);
        }
        if (player.getX() > this.getX() - (this.sprite.getWidth() / 2) - 80 && player.getX() < this.getX() + (this.sprite.getWidth() / 2) + 80 &&
                player.getY() > this.getY() - (this.sprite.getHeight() / 2) - 80 && player.getY() < this.getY() + (this.sprite.getHeight() / 2) + 8) {
            this.near = true;
        } else {
            this.near = false;
        }
    }

    /**
     * Custom render function that renders conditionally and with text.
     *
     * @param spriteBatch spritebatch to use for rendering
     */
    @Override
    public void render(SpriteBatch spriteBatch) {
        if (!this.collected) {
            super.render(spriteBatch);
            if (this.near) {
                font.draw(spriteBatch, "Key for " + doorId, getX(), getY() - 7.5f);
            }
        }
    }
}
