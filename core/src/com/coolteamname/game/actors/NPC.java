package com.coolteamname.game.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;

/**
 * Class to represent NPCs and allow then to "talk"
 */
public class NPC extends Actor {

    private String dialogue;
    private boolean showDialogue = false;

    /**
     * Constructor for each NPC instance
     *
     * @param xPos     initial x position
     * @param yPos     initial y position
     * @param texture  NPC texture
     * @param dialogue dialogue for each NPC
     */
    public NPC(float xPos, float yPos, Texture texture, String dialogue) {
        super(texture, xPos, yPos);
        this.dialogue = dialogue;
    }

    /**
     * Update the NPC each frame to rotate towards player and display text if need be
     *
     * @param player
     */
    public void update(Player player) {
        // Rotate NPC towards Player
        Vector3 playerPos = new Vector3(player.getX(), player.getY(), 0);
        float angle = MathUtils.radiansToDegrees *
                MathUtils.atan2(playerPos.y - this.sprite.getY(), playerPos.x - sprite.getX());
        this.sprite.setRotation(angle);

        if (player.getPlayerRectangle().overlaps(collisionRectangle))
            showDialogue = true;
        else
            showDialogue = false;
    }

    /**
     * Custom render function that conditionally renders the NPC dialogue
     *
     * @param spriteBatch spritebatch to use for rendering
     */
    @Override
    public void render(SpriteBatch spriteBatch) {
        super.render(spriteBatch);
        if (showDialogue) {
            font.draw(spriteBatch, dialogue, getX() - 100, getY() + 75);
        }
    }
}
