package com.coolteamname.game.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * Abstract class containing variables common to all actors within the game world.
 */
public abstract class Actor {
    protected Sprite sprite;
    protected Rectangle collisionRectangle;
    protected BitmapFont font;

    /**
     * Super class abstract constructor.
     *
     * @param texture Texture to be applied to the sprite
     * @param xPos    The initial x position of the actor
     * @param yPos    The initial y position of the actor
     */
    public Actor(Texture texture, float xPos, float yPos) {
        this.sprite = new Sprite(texture);
        this.sprite.setPosition(xPos, yPos);
        this.collisionRectangle = new Rectangle();
        this.collisionRectangle.set(xPos, yPos, texture.getWidth(), texture.getHeight());
        this.font = new BitmapFont();
    }

    /**
     * Returns the current x value of the sprite.
     *
     * @return the current x value of the sprite
     */
    public float getX() {
        return this.sprite.getX();
    }

    /**
     * Returns the current y value of the sprite.
     *
     * @return the current y value of the sprite
     */
    public float getY() {
        return this.sprite.getY();
    }

    /**
     * Returns the current collision rectangle.
     *
     * @return the current collision rectangle
     */
    public Rectangle getCollisionRectangle() {
        return this.collisionRectangle;
    }

    /**
     * Default render operation for actors that just need the sprite to be rendered.
     *
     * @param spriteBatch spritebatch to use for rendering
     */
    public void render(SpriteBatch spriteBatch) {
        sprite.draw(spriteBatch);
    }
}
