package com.coolteamname.game.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Class to represent each bullet object and hold associated information.
 */
public class Bullet extends Actor {
    private Vector2 direction;
    private float bulletSpeed = 1500.0f;
    private boolean finished = false;
    private boolean playerOrigin;

    /**
     * Constructor for each new bullet object.
     *
     * @param x            initial x position of the bullet
     * @param y            initial y position of the bullet
     * @param angle        angle to rotate the sprite
     * @param direction    direction vector to calculate bullet movement
     * @param texture      bullet texture
     * @param playerOrigin was the bullet shot by the player
     */
    public Bullet(float x, float y, float angle, Vector2 direction, Texture texture, boolean playerOrigin) {
        super(texture, x, y);
        this.direction = direction;
        this.sprite.setRotation(angle);
        this.playerOrigin = playerOrigin;
        if (!playerOrigin)
            bulletSpeed *= 0.5f;
    }

    /**
     * Updates the bullet each frame, moving the bullet and checking for collisions to destroy the bullet.
     *
     * @param collisionObjects collision objects to check if the bullets should be destroyed
     */
    public void update(MapObjects collisionObjects) {
        sprite.setPosition(sprite.getX() + bulletSpeed * Gdx.graphics.getDeltaTime() * direction.x,
                sprite.getY() + bulletSpeed * Gdx.graphics.getDeltaTime() * direction.y);
        collisionRectangle.setX(sprite.getX());
        collisionRectangle.setY(sprite.getY());

        // Collision Detection
        for (RectangleMapObject rectangleMapObject : collisionObjects.getByType(RectangleMapObject.class)) {
            Rectangle rectangle = rectangleMapObject.getRectangle();
            if (rectangle.overlaps(collisionRectangle))
                finished = true;
        }
    }

    /**
     * Returns whether the bullet has been destroyed.
     *
     * @return whether the bullet has been destroyed
     */
    public boolean isFinished() {
        return finished;
    }

    /**
     * Marks the bullet as destroyed
     */
    public void setFinished() {
        finished = true;
    }

    /**
     * Returns true if the player shot the bullet, otherwise returns false
     *
     * @return whether the player shot the bullet
     */
    public boolean isPlayerOrigin() {
        return playerOrigin;
    }
}
