package com.coolteamname.game.actors;

import com.badlogic.gdx.graphics.Texture;

/**
 * Class to represent Records
 */
public class RecordPickup extends Actor {

    /**
     * Constructor for each record instance
     *
     * @param xPos    initial x position
     * @param yPos    initial y position
     * @param texture texture of a record pickup
     */
    public RecordPickup(float xPos, float yPos, Texture texture) {
        super(texture, xPos, yPos);
        this.sprite.setScale(0.6f);
    }
}
