package com.coolteamname.game.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Class to represent each door in the game and hold the door state.
 */
public class Door extends Actor {

    private String doorId;
    private boolean unlocked;
    private boolean opened;
    private boolean near;

    /**
     * Constructor for each door instance.
     *
     * @param xPos    initial x position
     * @param yPos    initial y position
     * @param texture door texture
     * @param doorId  door identifier to link to key
     */
    public Door(float xPos, float yPos, Texture texture, String doorId) {
        super(texture, xPos, yPos);
        this.doorId = doorId;
        this.unlocked = false;
        this.opened = false;
        this.near = false;
    }

    /**
     * Getter for opened variable
     *
     * @return whether or not the door is open
     */
    public boolean isOpened() {
        return opened;
    }

    /**
     * Set the door to be unlocked
     */
    public void unlock() {
        this.unlocked = true;
    }

    /**
     * Updates the door each frame, checks if player is near to display text and checks if the door is opened by player
     *
     * @param player the player object
     */
    public void update(Player player) {
        if (player.getX() > this.getX() - (this.sprite.getWidth() / 2) - 60 && player.getX() < this.getX() + (this.sprite.getWidth() / 2) + 60 &&
                player.getY() > this.getY() - (this.sprite.getHeight() / 2) - 60 && player.getY() < this.getY() + (this.sprite.getHeight() / 2) + 60) {
            this.near = true;
            if (Gdx.input.isKeyPressed(Input.Keys.SPACE) && this.unlocked) {
                this.opened = true;
            }
        } else {
            this.near = false;
        }
    }

    /**
     * Custom render function that renders text when the player is near the door
     *
     * @param spriteBatch spritebatch to use for rendering
     */
    @Override
    public void render(SpriteBatch spriteBatch) {
        if (!this.opened) {
            super.render(spriteBatch);
            if (this.near) {
                if (!this.unlocked)
                    this.font.draw(spriteBatch, "Locked - " + doorId, getX(), getY());
                if (this.unlocked)
                    this.font.draw(spriteBatch, "Unlocked - Press SPACE to open", getX() - 40, getY() - 7.5f);
            }
        }
    }
}
