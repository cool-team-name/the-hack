package com.coolteamname.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.coolteamname.game.Application;

/**
 * Class to represent the Game Win Screen
 */
public class GameWinScreen implements Screen {

    final Application game;

    AssetManager manager;
    OrthographicCamera camera;

    Sprite background;
    BitmapFont titleFont;
    BitmapFont font;

    /**
     * Constructor for the GameWinScreen class
     *
     * @param game reference to the game
     */
    public GameWinScreen(Application game) {
        this.game = game;

        manager = new AssetManager();
        manager.load("background.png", Texture.class);
        manager.load("menu.mp3", Music.class);

        FileHandleResolver resolver = new InternalFileHandleResolver();
        manager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        manager.setLoader(BitmapFont.class, ".TTF", new FreetypeFontLoader(resolver));

        FreetypeFontLoader.FreeTypeFontLoaderParameter myBigFont = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        myBigFont.fontFileName = "DYMAXION.TTF";
        myBigFont.fontParameters.size = 100;
        manager.load("DYMAXION.TTF", BitmapFont.class, myBigFont);

        manager.finishLoading();

        background = new Sprite(manager.get("background.png", Texture.class));
        background.setScale(0.9f);
        background.setPosition(-600, -300);

        titleFont = manager.get("DYMAXION.TTF", BitmapFont.class);
        font = new BitmapFont();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    @Override
    public void show() {

    }

    /**
     * Custom render method to render the game win screen
     *
     * @param delta time delta since last frame
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.getSpriteBatch().setProjectionMatrix(camera.combined);

        game.getSpriteBatch().begin();
        background.draw(game.getSpriteBatch());
        titleFont.draw(game.getSpriteBatch(), "DJ MC Wins!", 175, 500);
        font.draw(game.getSpriteBatch(), "Click to play again!", 350, 400);
        game.getSpriteBatch().end();

        if (Gdx.input.isTouched()) {
            game.setScreen(new GameScreen(game));
            dispose();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
