package com.coolteamname.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.coolteamname.game.Application;
import com.coolteamname.game.actors.Player;
import com.coolteamname.game.managers.*;

/**
 * Class to represent the main game
 */
public class GameScreen implements Screen {

    private Application game;

    private AssetManager manager;
    private OrthographicCamera camera;
    private OrthographicCamera guiCamera;
    private OrthogonalTiledMapRenderer renderer;

    private Music music;
    private BitmapFont font;

    private LevelManager levelManager;
    private Player player;
    private BulletManager bulletManager;
    private EnemyManager enemyManager;
    private PickupManager pickupManager;
    private StatManager statManager;
    private NPCManager npcManager;
    private KeycardManager keycardManager;
    private DoorManager doorManager;
    private PowerupManager powerupManager;
    private GameState currentState = GameState.RUNNING;

    /**
     * Constructor for the main game
     *
     * @param application reference to the application object
     */
    public GameScreen(Application application) {

        this.game = application;

        // Background Music
        music = Gdx.audio.newMusic(Gdx.files.internal("level.mp3"));
        music.setLooping(true);
        music.play();

        // Load Assets
        manager = new AssetManager();
        manager.load("player.png", Texture.class);
        manager.load("enemy.png", Texture.class);
        manager.load("bullet.png", Texture.class);
        manager.load("pickup.png", Texture.class);
        manager.load("health.png", Texture.class);
        manager.load("overlay.png", Texture.class);
        manager.load("door.png", Texture.class);
        manager.load("keycard.png", Texture.class);
        manager.load("npc.png", Texture.class);
        manager.load("powerup.png", Texture.class);
        manager.load("powerup_title.png", Texture.class);
        manager.load("paused.png", Texture.class);

        manager.finishLoading();

        font = new BitmapFont();

        // Game Managers
        statManager = new StatManager();
        player = new Player(500, 350, manager.get("player.png", Texture.class), manager.get("health.png", Texture.class), statManager);
        enemyManager = new EnemyManager(manager.get("enemy.png", Texture.class), manager.get("health.png", Texture.class), statManager);
        bulletManager = new BulletManager(manager.get("bullet.png", Texture.class), Gdx.audio.newSound(Gdx.files.internal("gunshot.wav")));
        pickupManager = new PickupManager(manager.get("pickup.png", Texture.class), 3);
        npcManager = new NPCManager(manager.get("npc.png", Texture.class));
        keycardManager = new KeycardManager(manager.get("keycard.png", Texture.class));
        doorManager = new DoorManager(manager.get("door.png", Texture.class));
        powerupManager = new PowerupManager(manager.get("powerup.png", Texture.class), manager.get("powerup_title.png", Texture.class));
        levelManager = new LevelManager(enemyManager, pickupManager, player, npcManager, keycardManager, doorManager, powerupManager, manager.get("overlay.png", Texture.class));

        // Set up camera and tilemap renderer
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.x = levelManager.getMapWidthInPixels() * .5f;
        camera.position.y = levelManager.getMapHeightInPixels() * .35f;
        renderer = new OrthogonalTiledMapRenderer(levelManager.getMap());

        // Set up GUI camera
        guiCamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        game.getFont().getData().setScale(1.2f);

    }

    @Override
    public void show() {

    }

    /**
     * Method to perform render operations for the RUNNING state
     *
     * @param delta time delta since last frame
     */
    private void renderRunning(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            currentState = GameState.PAUSED;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.TAB)) {
            bulletManager.cycleWeapon();
        }

        // Clear Screen
        Gdx.gl.glClearColor(.5f, .7f, .9f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Render Tilemap
        camera.update();
        renderer.setView(camera);
        renderer.render();

        bulletManager.updateBullets(levelManager.getCollisionObjects());
        player.update(camera, bulletManager, levelManager.getCollisionObjects(), doorManager);
        enemyManager.updateEnemies(player, bulletManager);
        pickupManager.updatePickups(player);
        npcManager.updateNPCs(player);
        keycardManager.update(player, doorManager);
        doorManager.update(player);
        powerupManager.update(player, bulletManager);

        // Update Camera Position
        camera.position.x = player.getX();
        camera.position.y = player.getY();

        // Render Game
        game.getSpriteBatch().setProjectionMatrix(camera.combined);
        game.getSpriteBatch().begin();

        bulletManager.renderBullets(game.getSpriteBatch());
        player.render(game.getSpriteBatch());
        enemyManager.renderEnemies(game.getSpriteBatch());
        pickupManager.renderPickups(game.getSpriteBatch());
        npcManager.renderNPCs(game.getSpriteBatch());
        keycardManager.render(game.getSpriteBatch());
        doorManager.render(game.getSpriteBatch());
        powerupManager.render(game.getSpriteBatch());

        // Render GUI
        game.getSpriteBatch().setProjectionMatrix(guiCamera.combined);
        String score = "Records: " + pickupManager.getCollected() + " / " + pickupManager.getTotalPickups();

        if (levelManager.isIntroMode()) {
            levelManager.renderLevelOverlay(game.getSpriteBatch());
            if (Gdx.input.isTouched()) {
                levelManager.setIntroMode(false);
            }
        }

        if (levelManager.isOutroMode()) {
            levelManager.renderLevelOverlay(game.getSpriteBatch());
            if (Gdx.input.isTouched()) {
                reset();
            }
        }

        // Check if player is dead
        if (player.isDead()) {
            music.stop();
            game.setScreen(new GameOverScreen(game));
        }

        // Check for game end and display text
        if (pickupManager.isGameFinished()) {
            levelManager.setOutroMode(true);
        } else {
            font.draw(game.getSpriteBatch(), score, -385, -300);
            font.draw(game.getSpriteBatch(), bulletManager.currentWeapon(), -10, -22);
            font.draw(game.getSpriteBatch(), keycardManager.getKeycardUIText(), -385, 150);
            powerupManager.renderTitle(game.getSpriteBatch());
        }

        game.getSpriteBatch().end();
    }

    /**
     * Method to perform render operations for the PAUSED state
     *
     * @param delta time delta since last frame
     */
    private void renderPaused(float delta) {
        game.getSpriteBatch().setProjectionMatrix(guiCamera.combined);

        Texture pauseTitle = manager.get("paused.png", Texture.class);
        game.getSpriteBatch().begin();
        game.getSpriteBatch().draw(pauseTitle, 0 - (pauseTitle.getWidth() / 2), 0);

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE) || Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            currentState = GameState.RUNNING;
        }

        game.getSpriteBatch().end();
    }

    /**
     * Custom render method to render based on state
     *
     * @param delta time delta since last frame
     */
    @Override
    public void render(float delta) {

        switch (currentState) {
            case RUNNING:
                renderRunning(delta);
                break;
            case PAUSED:
                renderPaused(delta);
                break;
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    /**
     * Custom dispose method that disposes of all loaded assets
     */
    @Override
    public void dispose() {
        music.dispose();
        manager.dispose();
    }

    /**
     * Reset method to reload the game from the beginning
     */
    public void reset() {
        levelManager.reset();
        player.reset(500, 350);
        pickupManager.reset();
        if (levelManager.getCurrentLevel() == 3) {
            music.stop();
            game.setScreen(new GameWinScreen(game));
        } else {
            levelManager.initLevel(levelManager.getCurrentLevel() + 1);
            renderer = new OrthogonalTiledMapRenderer(levelManager.getMap());
        }
    }

    private enum GameState {
        INTRO,
        RUNNING,
        PAUSED,
        INVENTORY,
        NOTEBOOK,
        OUTRO
    }
}
