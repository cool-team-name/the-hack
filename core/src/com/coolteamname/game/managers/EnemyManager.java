package com.coolteamname.game.managers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.coolteamname.game.actors.Enemy;
import com.coolteamname.game.actors.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for managing all enemies within the game.
 */
public class EnemyManager {
    private List<Enemy> enemyList;
    private Texture enemyTexture;
    private Texture healthBarTexture;
    private StatManager statManager;

    /**
     * Constructor for EnemyManager.
     *
     * @param texture          texture of enemies
     * @param healthBarTexture texture of healthbar
     * @param statManager      reference to stat manager
     */
    public EnemyManager(Texture texture, Texture healthBarTexture, StatManager statManager) {
        enemyList = new ArrayList<Enemy>();
        enemyTexture = texture;
        this.healthBarTexture = healthBarTexture;
        this.statManager = statManager;
    }

    /**
     * Add a new enemy to the world, used by LevelManager.
     *
     * @param x x position
     * @param y y position
     */
    public void addEnemy(float x, float y) {
        enemyList.add(new Enemy(x, y, enemyTexture, healthBarTexture, statManager));
    }

    /**
     * Loop through all enemies and call update method.
     *
     * @param player        reference to player
     * @param bulletManager reference to bulletManager
     */
    public void updateEnemies(Player player, BulletManager bulletManager) {
        List<Enemy> deadEnemies = new ArrayList<Enemy>();
        for (Enemy enemy : enemyList) {
            enemy.update(player, bulletManager);
            if (enemy.isDead())
                deadEnemies.add(enemy);
        }

        for (Enemy deadEnemy : deadEnemies) {
            enemyList.remove(deadEnemy);
        }
    }

    /**
     * Loop through all enemies and call render method.
     *
     * @param spriteBatch reference to spritebatch to use to render enemies
     */
    public void renderEnemies(SpriteBatch spriteBatch) {
        for (Enemy enemy : enemyList) {
            enemy.render(spriteBatch);
        }
    }
}
