package com.coolteamname.game.managers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.coolteamname.game.actors.Keycard;
import com.coolteamname.game.actors.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for managing keycards in the game
 */
public class KeycardManager {

    private List<Keycard> keycardList;
    private Texture keycardTexture;

    /**
     * Constructor for KeycardManager
     *
     * @param texture keycard texture
     */
    public KeycardManager(Texture texture) {
        keycardList = new ArrayList<Keycard>();
        this.keycardTexture = texture;
    }

    public String getKeycardUIText() {
        String text = "Keycards:\n";
        for (Keycard keycard : keycardList) {
            if (keycard.isCollected()) {
                text += "     - ";
                text += keycard.getDoorId();
                text += "\n";
            }
        }
        return text;
    }

    /**
     * Method to add new keycard to the world.
     *
     * @param x      x position
     * @param y      y position
     * @param doorId the id of the door which the keycard unlocks
     */
    public void addKeycard(float x, float y, String doorId) {
        keycardList.add(new Keycard(x, y, keycardTexture, doorId));
    }

    /**
     * Loop through all keycards and call update method.
     *
     * @param player      reference to player
     * @param doorManager reference to doorManager
     */
    public void update(Player player, DoorManager doorManager) {
        for (Keycard keycard : keycardList) {
            keycard.update(player, doorManager);
        }
    }

    /**
     * Loop through all keycards and call render method.
     *
     * @param spriteBatch reference to spriteBatch to render keycards
     */
    public void render(SpriteBatch spriteBatch) {
        for (Keycard keycard : keycardList) {
            keycard.render(spriteBatch);
        }
    }
}
