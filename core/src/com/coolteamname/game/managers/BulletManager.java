package com.coolteamname.game.managers;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.math.Vector2;
import com.coolteamname.game.actors.Bullet;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class for managing all in game bullets and weapons and updating their state based on actions
 */
public class BulletManager {
    private List<Bullet> bulletList;
    private Texture bulletTexture;
    private Sound bulletSound;
    private Random random;
    private Weapon currentWeapon = Weapon.MACHINE_GUN;
    private float shootDelay = 0.35f;
    private boolean multiShot;

    /**
     * Constructor for BulletManager instance.
     *
     * @param texture     bullet texture
     * @param bulletSound bullet sound
     */
    public BulletManager(Texture texture, Sound bulletSound) {
        this.bulletList = new ArrayList<Bullet>();
        this.bulletTexture = texture;
        this.bulletSound = bulletSound;
        this.random = new Random();
        this.multiShot = false;
    }

    /**
     * Getter for the current delay between shots.
     *
     * @return the current delay between shots depending on weapon
     */
    public float getShootDelay() {
        return shootDelay;
    }

    /**
     * Method that returns the current weapon as a human readable string
     *
     * @return
     */
    public String currentWeapon() {
        switch (currentWeapon) {
            case MACHINE_GUN:
                return "Machine Gun";
            case SHOTGUN:
                return "Shotgun";
            case PISTOL:
                return "Pistol";
            case SNIPER_RIFLE:
                return "Sniper Rifle";
            default:
                return "Uhhhh it's broken";
        }
    }

    /**
     * Returns the list of all bullets, useful for collision detection.
     *
     * @return the list of all bullet objects in the game
     */
    public List<Bullet> getBulletList() {
        return bulletList;
    }

    /**
     * Setter that enables/disables multishot mode for powerups.
     *
     * @param multiShot boolean if enabling mutlishot
     */
    public void setMultiShot(boolean multiShot) {
        this.multiShot = multiShot;
    }

    /**
     * Seperate method to allow enemies to shoot to prevent player powerups and weapon changes from affecting enemies.
     *
     * @param x            initial bullet x position
     * @param y            initial bullet y position
     * @param angle        initial bullet angle
     * @param direction    initial bullet direction
     * @param playerOrigin shot by player or not (Unnecessary, should change)
     */
    public void enemyShootBullet(float x, float y, float angle, Vector2 direction, boolean playerOrigin) {
        bulletList.add(new Bullet(x, y, angle, direction, bulletTexture, playerOrigin));
        long soundId = bulletSound.play(0.4f);
    }

    /**
     * Standard shoot method to allow player to shoot
     *
     * @param x            initial bullet x position
     * @param y            initial bullet y position
     * @param angle        initial bullet angle
     * @param direction    initial bullet direction
     * @param playerOrigin shot by player or not (Unnecessary, should change)
     */
    public void shootBullet(float x, float y, float angle, Vector2 direction, boolean playerOrigin) {
        long soundId;
        switch (currentWeapon) {
            case MACHINE_GUN:
                bulletList.add(new Bullet(x, y, angle, direction, bulletTexture, playerOrigin));
                if (multiShot)
                    multishot(x, y, angle, direction, playerOrigin);
                soundId = bulletSound.play(0.4f);
                break;
            case SHOTGUN:
                bulletList.add(new Bullet(x, y, angle, direction.cpy(), bulletTexture, playerOrigin));
                bulletList.add(new Bullet(x, y, angle, direction.cpy().rotate(10), bulletTexture, playerOrigin));
                bulletList.add(new Bullet(x, y, angle, direction.cpy().rotate(-10), bulletTexture, playerOrigin));
                if (multiShot)
                    multishot(x, y, angle, direction, playerOrigin);
                soundId = bulletSound.play(0.4f);
                break;
            case PISTOL:
                bulletList.add(new Bullet(x, y, angle, direction, bulletTexture, playerOrigin));
                if (multiShot)
                    multishot(x, y, angle, direction, playerOrigin);
                soundId = bulletSound.play(0.4f);
                break;
            case SNIPER_RIFLE:
                bulletList.add(new Bullet(x, y, angle, direction, bulletTexture, playerOrigin));
                if (multiShot)
                    multishot(x, y, angle, direction, playerOrigin);
                soundId = bulletSound.play(0.4f);
                break;
        }
    }

    private void multishot(float x, float y, float angle, Vector2 direction, boolean playerOrigin) {
        bulletList.add(new Bullet(x, y, angle, direction.cpy().rotate(90), bulletTexture, playerOrigin));
        bulletList.add(new Bullet(x, y, angle, direction.cpy().rotate(180), bulletTexture, playerOrigin));
        bulletList.add(new Bullet(x, y, angle, direction.cpy().rotate(270), bulletTexture, playerOrigin));

    }

    /**
     * Cycles to the next weapon.
     */
    public void cycleWeapon() {
        switch (currentWeapon) {
            case MACHINE_GUN:
                currentWeapon = Weapon.SHOTGUN;
                shootDelay = 1.2f;
                break;
            case SHOTGUN:
                currentWeapon = Weapon.PISTOL;
                shootDelay = 0.8f;
                break;
            case PISTOL:
                currentWeapon = Weapon.SNIPER_RIFLE;
                shootDelay = 1.2f;
                break;
            case SNIPER_RIFLE:
                currentWeapon = Weapon.MACHINE_GUN;
                shootDelay = 0.25f;
                break;
        }
    }

    /**
     * Loops through all bullets and calls the update function for each bullet.
     *
     * @param collisionObjects List of all wall objects to check for coliision
     */
    public void updateBullets(MapObjects collisionObjects) {
        List<Bullet> finishedBullets = new ArrayList<Bullet>();
        for (Bullet bullet : bulletList) {
            if (bullet.isFinished())
                finishedBullets.add(bullet);
            bullet.update(collisionObjects);
        }

        for (Bullet bullet : finishedBullets) {
            bulletList.remove(bullet);
        }
    }

    /**
     * Calls render function on each bullet.
     *
     * @param spriteBatch the spritebatch to be used to render each bullet
     */
    public void renderBullets(SpriteBatch spriteBatch) {
        for (Bullet bullet : bulletList) {
            bullet.render(spriteBatch);
        }
    }

    /**
     * All possible weapon variations
     */
    private enum Weapon {
        MACHINE_GUN,
        SHOTGUN,
        PISTOL,
        SNIPER_RIFLE
    }
}
