package com.coolteamname.game.managers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Rectangle;
import com.coolteamname.game.actors.Player;

/**
 * Class for LevelManager used for constructing levels from tiled maps
 */
public class LevelManager {

    private int currentLevel;

    private int tileWidth, tileHeight,
            mapWidthInTiles, mapHeightInTiles,
            mapWidthInPixels, mapHeightInPixels;

    private String intro, outro;
    private Sprite overlaySprite;
    private BitmapFont font;
    private boolean introMode = true;
    private boolean outroMode = false;

    private TmxMapLoader mapLoader;
    private TiledMap map;
    private MapObjects collisionObjects;
    private MapObjects pickupObjects;
    private MapObjects playerObjects;
    private MapObjects npcObjects;
    private MapObjects doorObjects;
    private MapObjects keycardObjects;
    private MapObjects powerupObjects;

    private EnemyManager enemyManager;
    private PickupManager pickupManager;
    private NPCManager npcManager;
    private KeycardManager keycardManager;
    private DoorManager doorManager;
    private PowerupManager powerupManager;
    private Player player;

    /**
     * Constructor for LevelManager
     *
     * @param enemyManager   reference to enemyManager
     * @param pickupManager  reference to pickupManager
     * @param player         reference to player
     * @param npcManager     reference to NPCManager
     * @param keycardManager reference to KeycardManager
     * @param doorManager    reference to DoorManager
     * @param powerupManager reference to PowerupManager
     * @param overlayTexture overlayTexture for text
     */
    public LevelManager(EnemyManager enemyManager,
                        PickupManager pickupManager,
                        Player player,
                        NPCManager npcManager,
                        KeycardManager keycardManager,
                        DoorManager doorManager,
                        PowerupManager powerupManager,
                        Texture overlayTexture) {
        this.mapLoader = new TmxMapLoader();
        this.enemyManager = enemyManager;
        this.pickupManager = pickupManager;
        this.npcManager = npcManager;
        this.keycardManager = keycardManager;
        this.doorManager = doorManager;
        this.powerupManager = powerupManager;
        this.player = player;
        this.overlaySprite = new Sprite(overlayTexture);
        this.overlaySprite.setScale(20);
        this.overlaySprite.setPosition(0, 0);
        this.font = new BitmapFont();

        initLevel(1);
    }

    /**
     * Method to initialise a level based on a level number and the associated tiled map
     *
     * @param levelNumber the number of the level to load
     */
    public void initLevel(int levelNumber) {

        this.currentLevel = levelNumber;

        if (this.map != null)
            this.reset();

        // Load Selected Level
        this.map = this.mapLoader.load("level-" + levelNumber + ".tmx");

        // Get Map Properties
        MapProperties mapProperties = map.getProperties();
        tileWidth = mapProperties.get("tilewidth", Integer.class);
        tileHeight = mapProperties.get("tileheight", Integer.class);
        mapWidthInTiles = mapProperties.get("width", Integer.class);
        mapHeightInTiles = mapProperties.get("height", Integer.class);
        mapWidthInPixels = tileWidth * mapWidthInTiles;
        mapHeightInPixels = tileHeight * mapHeightInTiles;
        intro = mapProperties.get("intro", String.class);
        outro = mapProperties.get("outro", String.class);

        // Get Collision Data
        int collisionLayerId = 1;
        MapLayer collisionLayer = map.getLayers().get(collisionLayerId);
        collisionObjects = collisionLayer.getObjects();

        // Get Enemy Locations
        int enemyLayerId = 3;
        MapLayer enemyLayer = map.getLayers().get(enemyLayerId);
        pickupObjects = enemyLayer.getObjects();

        // Spawn Enemies
        for (RectangleMapObject rectangleMapObject : pickupObjects.getByType(RectangleMapObject.class)) {
            Rectangle rectangle = rectangleMapObject.getRectangle();
            enemyManager.addEnemy(rectangle.getX(), rectangle.getY());
        }

        // Get Pickup Locations
        int pickupLayerId = 4;
        MapLayer pickupLayer = map.getLayers().get(pickupLayerId);
        pickupObjects = pickupLayer.getObjects();

        // Spawn Pickups
        for (RectangleMapObject rectangleMapObject : pickupObjects.getByType(RectangleMapObject.class)) {
            Rectangle rectangle = rectangleMapObject.getRectangle();
            pickupManager.addPickup(rectangle.getX(), rectangle.getY());
        }
        pickupManager.setTotal();

        // Get Player Spawn Point
        int playerLayerId = 5;
        MapLayer playerLayer = map.getLayers().get(playerLayerId);
        playerObjects = playerLayer.getObjects();

        // Spawn Player
        for (RectangleMapObject rectangleMapObject : playerObjects.getByType(RectangleMapObject.class)) {
            Rectangle rectangle = rectangleMapObject.getRectangle();
            player.setPos(rectangle.getX(), rectangle.getY());
        }

        // Get NPC Locations
        int npcLayerId = 6;
        MapLayer npcLayer = map.getLayers().get(npcLayerId);
        npcObjects = npcLayer.getObjects();

        // Spawn NPCs
        for (RectangleMapObject rectangleMapObject : npcObjects.getByType(RectangleMapObject.class)) {
            Rectangle rectangle = rectangleMapObject.getRectangle();
            String dialogue = rectangleMapObject.getProperties().get("dialogue").toString();
            npcManager.addNPC(rectangle.getX(), rectangle.getY(), dialogue);
        }

        // Get Door Locations
        int doorLayerId = 7;
        MapLayer doorLayer = map.getLayers().get(doorLayerId);
        doorObjects = doorLayer.getObjects();

        // Spawn Doors
        for (RectangleMapObject rectangleMapObject : doorObjects.getByType(RectangleMapObject.class)) {
            Rectangle rectangle = rectangleMapObject.getRectangle();
            String doorId = rectangleMapObject.getProperties().get("doorId").toString();
            doorManager.addDoor(rectangle.getX(), rectangle.getY(), doorId);
        }

        // Get Keycard Locations
        int keycardLayerId = 8;
        MapLayer keycardLayer = map.getLayers().get(keycardLayerId);
        keycardObjects = keycardLayer.getObjects();

        // Spawn Keycards
        for (RectangleMapObject rectangleMapObject : keycardObjects.getByType(RectangleMapObject.class)) {
            Rectangle rectangle = rectangleMapObject.getRectangle();
            String doorId = rectangleMapObject.getProperties().get("doorId").toString();
            keycardManager.addKeycard(rectangle.getX(), rectangle.getY(), doorId);
        }

        // Get Powerup Locations
        int powerupLayerId = 9;
        MapLayer powerupLayer = map.getLayers().get(powerupLayerId);
        powerupObjects = powerupLayer.getObjects();

        // Spawn Powerups
        for (RectangleMapObject rectangleMapObject : powerupObjects.getByType(RectangleMapObject.class)) {
            Rectangle rectangle = rectangleMapObject.getRectangle();
            boolean invincible = (Boolean) rectangleMapObject.getProperties().get("invincible");
            boolean reducedShootTimer = (Boolean) rectangleMapObject.getProperties().get("reducedShootTimer");
            boolean multiShot = (Boolean) rectangleMapObject.getProperties().get("multiShot");
            int duration = (Integer) rectangleMapObject.getProperties().get("duration");
            powerupManager.add(rectangle.getX(), rectangle.getY(), duration, invincible, reducedShootTimer, multiShot);
        }

    }

    /**
     * Resets the level
     */
    public void reset() {
        introMode = true;
        outroMode = false;
        map.dispose();
    }

    /**
     * Getter for map width in pixels
     *
     * @return map width in pixels
     */
    public int getMapWidthInPixels() {
        return mapWidthInPixels;
    }

    /**
     * Getter for map height in pixels
     *
     * @return map height in pixels
     */
    public int getMapHeightInPixels() {
        return mapHeightInPixels;
    }

    /**
     * Getter for current tiled map
     *
     * @return current tiled map
     */
    public TiledMap getMap() {
        return map;
    }

    /**
     * Getter for all collision objects in the map
     *
     * @return all collision objects in the map
     */
    public MapObjects getCollisionObjects() {
        return collisionObjects;
    }

    /**
     * Getter for current level number
     *
     * @return current level number
     */
    public int getCurrentLevel() {
        return currentLevel;
    }

    /**
     * Getter for current level intro text.
     *
     * @return current level intro text
     */
    public String getIntro() {
        return intro;
    }

    /**
     * Getter for current level outro text
     *
     * @return current level outro text
     */
    public String getOutro() {
        return outro;
    }

    /**
     * Method to render intro overlay
     *
     * @param spriteBatch reference to spritebatch
     */
    public void renderLevelOverlay(SpriteBatch spriteBatch) {
        overlaySprite.draw(spriteBatch);
        if (introMode)
            font.draw(spriteBatch, intro, -300, 100);
        if (outroMode)
            font.draw(spriteBatch, outro, -300, 100);
    }

    /**
     * Checks is currently in intro mode
     *
     * @return if in intro mode
     */
    public boolean isIntroMode() {
        return introMode;
    }

    /**
     * Setter for intro mode
     *
     * @param introMode boolean value
     */
    public void setIntroMode(boolean introMode) {
        this.introMode = introMode;
    }

    /**
     * Getter for outro mode
     *
     * @return is outro mode
     */
    public boolean isOutroMode() {
        return outroMode;
    }

    /**
     * Setter for outro mode
     *
     * @param outroMode boolean value
     */
    public void setOutroMode(boolean outroMode) {
        this.outroMode = outroMode;
    }
}
