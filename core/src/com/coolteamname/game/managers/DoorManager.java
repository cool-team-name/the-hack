package com.coolteamname.game.managers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.coolteamname.game.actors.Door;
import com.coolteamname.game.actors.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Class for managing all doors within the game world
 */
public class DoorManager {

    private HashMap<String, Door> doorList;
    private Texture doorTexture;

    /**
     * Constructor for DoorManager instances
     *
     * @param texture texture for doors
     */
    public DoorManager(Texture texture) {
        doorList = new HashMap<String, Door>();
        this.doorTexture = texture;
    }

    /**
     * Returns a list of all door objects within the game
     *
     * @return List of all door objects within the game
     */
    public List<Door> getDoorList() {
        return new ArrayList<Door>(doorList.values());
    }

    /**
     * Creates a new door through the Level Manager
     *
     * @param x      x position for door
     * @param y      y position for door
     * @param doorId identifier for door to check for keycards
     */
    public void addDoor(float x, float y, String doorId) {
        doorList.put(doorId, new Door(x, y, doorTexture, doorId));
    }

    /**
     * Marks a door as unlocked based on its id
     *
     * @param doorId the id of the door to unlock
     */
    public void setUnlocked(String doorId) {
        doorList.get(doorId).unlock();
    }

    /**
     * Loops through all doors and calls the update function
     *
     * @param player reference to player object to be passed to the doors
     */
    public void update(Player player) {
        for (Door door : this.doorList.values()) {
            door.update(player);
        }
    }

    /**
     * Loops through all doors and calls the render function
     *
     * @param spriteBatch the spritebatch to be used to render the doors
     */
    public void render(SpriteBatch spriteBatch) {
        for (Door door : this.doorList.values()) {
            door.render(spriteBatch);
        }
    }
}
