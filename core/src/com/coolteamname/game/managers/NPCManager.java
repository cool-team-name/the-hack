package com.coolteamname.game.managers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.coolteamname.game.actors.NPC;
import com.coolteamname.game.actors.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to manage all NPCs within the game
 */
public class NPCManager {

    private List<NPC> npcList;
    private Texture npcTexture;

    /**
     * Constructor for NPC Manager
     *
     * @param texture texture of NPCs
     */
    public NPCManager(Texture texture) {
        npcList = new ArrayList<NPC>();
        npcTexture = texture;
    }

    /**
     * Adds a new NPC to the world
     *
     * @param x        x position of NPC
     * @param y        y position of NPC
     * @param dialogue NPC dialogue line
     */
    public void addNPC(float x, float y, String dialogue) {
        npcList.add(new NPC(x, y, npcTexture, dialogue));
    }

    /**
     * Loop through all NPCs and call update method
     *
     * @param player reference to the player
     */
    public void updateNPCs(Player player) {
        for (NPC npc : npcList) {
            npc.update(player);
        }
    }

    /**
     * Loop through all NPCs and call the render method
     *
     * @param spriteBatch reference to the spritebatch
     */
    public void renderNPCs(SpriteBatch spriteBatch) {
        for (NPC npc : npcList) {
            npc.render(spriteBatch);
        }
    }
}
