package com.coolteamname.game.managers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.coolteamname.game.actors.Player;
import com.coolteamname.game.actors.RecordPickup;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to manage pickups within the game
 */
public class PickupManager {
    private List<RecordPickup> pickupList;
    private Texture pickupTexture;

    private int collected = 0;
    private int total;

    /**
     * Constructor for PickupManger
     *
     * @param texture
     * @param total
     */
    public PickupManager(Texture texture, int total) {
        this.pickupTexture = texture;
        pickupList = new ArrayList<RecordPickup>();
        this.total = total;
    }

    /**
     * Getter for total number of collected pickups
     *
     * @return total number of collected pickups
     */
    public int getCollected() {
        return collected;
    }

    /**
     * Getter for total number of pickups
     *
     * @return total number of pickups
     */
    public int getTotalPickups() {
        return total;
    }

    /**
     * Method to reset the pickup manager
     */
    public void reset() {
        collected = 0;
        pickupList = new ArrayList<RecordPickup>();
    }

    /**
     * Setter for total number of pickups
     */
    public void setTotal() {
        this.total = pickupList.size();
    }

    /**
     * Adds a new pickup to the game
     *
     * @param x x position
     * @param y y position
     */
    public void addPickup(float x, float y) {
        pickupList.add(new RecordPickup(x, y, pickupTexture));
    }

    /**
     * Loop through all pickups and call update method
     *
     * @param player reference to player
     */
    public void updatePickups(Player player) {
        List<RecordPickup> remove = new ArrayList<RecordPickup>();
        for (RecordPickup recordPickup : pickupList) {
            if (recordPickup.getCollisionRectangle().overlaps(player.getPlayerRectangle())) {
                remove.add(recordPickup);
            }
        }

        // Avoid concurrency issues
        removePickups(remove);
    }

    /**
     * Loop through all pickups and call render method
     *
     * @param spriteBatch reference to the spritebatch
     */
    public void renderPickups(SpriteBatch spriteBatch) {
        for (RecordPickup pickup : pickupList) {
            pickup.render(spriteBatch);
        }
    }

    /**
     * Method for removing pickups from the list of pickups to prevent concurrency issues
     *
     * @param recordPickups list of pickups to be removed from the list
     */
    private void removePickups(List<RecordPickup> recordPickups) {
        for (RecordPickup recordPickup : recordPickups) {
            pickupList.remove(recordPickup);
            collected++;
        }
    }

    /**
     * Method to check if the player has collected the all of the pickups and the level is ended.
     *
     * @return whether the level is complete
     */
    public boolean isGameFinished() {
        if (collected == total) {
            return true;
        } else {
            return false;
        }
    }
}
