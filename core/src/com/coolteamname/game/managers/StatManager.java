package com.coolteamname.game.managers;

/**
 * Class for tracking a variety of stats throughout the game
 */
public class StatManager {

    private int playerBullets = 0;
    private int enemyBullets = 0;
    private int playerHits = 0;
    private int enemyHits = 0;

    /**
     * Getter for playerBullets
     *
     * @return number of player shots
     */
    public int getPlayerBullets() {
        return playerBullets;
    }

    /**
     * Getter for enemyBullets
     *
     * @return number of enemy shots
     */
    public int getEnemyBullets() {
        return enemyBullets;
    }

    /**
     * Getter for playerHits
     *
     * @return number of times the player hit an enemy
     */
    public int getPlayerHits() {
        return playerHits;
    }

    /**
     * Getter for enemyHits
     *
     * @return number of times the enemies hit the player
     */
    public int getEnemyHits() {
        return enemyHits;
    }

    /**
     * Getter for calculating player accuracy
     *
     * @return player accuracy
     */
    public double getPlayerAccuracy() {
        return (double) enemyHits / (double) playerBullets;
    }

    /**
     * Getter for calculating enemy accuracy
     *
     * @return enemy accuacy
     */
    public double getEnemyAccuracy() {
        return (double) playerHits / (double) enemyBullets;
    }


    /**
     * Method for recording a player shot
     */
    public void playerShoot() {
        playerBullets++;
    }

    /**
     * Method for recording an enemy shot
     */
    public void enemyShoot() {
        enemyBullets++;
    }

    /**
     * Method for recording a player hit
     */
    public void playerHit() {
        playerHits++;
    }

    /**
     * Method for recording an enemy hit
     */
    public void enemyHit() {
        enemyHits++;
    }
}
