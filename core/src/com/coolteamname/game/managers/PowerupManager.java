package com.coolteamname.game.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.coolteamname.game.actors.Player;
import com.coolteamname.game.actors.PowerupPickup;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to manage the powerups
 */
public class PowerupManager {

    private List<PowerupPickup> powerupList;
    private Texture powerupTexture;
    private Texture titleTexture;
    private boolean showTitle;
    private float timer;
    private float maxTime;

    /**
     * Constructor for powerup manager
     *
     * @param texture powerup texture
     */
    public PowerupManager(Texture texture, Texture titleTexture) {
        this.powerupList = new ArrayList<PowerupPickup>();
        this.powerupTexture = texture;
        this.titleTexture = titleTexture;
        this.showTitle = false;
        this.timer = 0.0f;
        this.maxTime = 1.0f;
    }

    /**
     * Method to add new pickup to the game
     *
     * @param xPos              x position
     * @param yPos              y position
     * @param duration          duration of powerup
     * @param invincible        is invincible
     * @param reducedShootTimer has reduced shoot timer
     * @param multiShot         can multishoot
     */
    public void add(float xPos, float yPos, int duration, boolean invincible, boolean reducedShootTimer, boolean multiShot) {
        powerupList.add(new PowerupPickup(powerupTexture, xPos, yPos, duration, invincible, reducedShootTimer, multiShot, this));
    }

    public void enableTitle() {
        showTitle = true;
    }

    /**
     * Loop through all powerups and call update method
     *
     * @param player        reference to player
     * @param bulletManager reference to bullet manager
     */
    public void update(Player player, BulletManager bulletManager) {
        for (PowerupPickup powerupPickup : this.powerupList) {
            powerupPickup.update(player, bulletManager);
        }
        if (showTitle) {
            timer += Gdx.graphics.getDeltaTime();
            if (timer >= maxTime) {
                showTitle = false;
                timer = 0.0f;
            }
        }
    }

    /**
     * Loop through all powerups and call render method
     *
     * @param spriteBatch reference to spritebatch
     */
    public void render(SpriteBatch spriteBatch) {
        for (PowerupPickup powerupPickup : this.powerupList) {
            powerupPickup.render(spriteBatch);
        }
    }

    public void renderTitle(SpriteBatch spriteBatch) {
        if (showTitle) {
            spriteBatch.draw(titleTexture, 0 - (titleTexture.getWidth() / 2), 0);
        }
        for (PowerupPickup powerupPickup : this.powerupList) {
            powerupPickup.renderTitle(spriteBatch);
        }
    }
}
