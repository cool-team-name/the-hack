package com.coolteamname.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.coolteamname.game.screens.MainMenuScreen;

public class Application extends Game {

    SpriteBatch spriteBatch;
    BitmapFont font;

    public SpriteBatch getSpriteBatch() {
        return spriteBatch;
    }

    public BitmapFont getFont() {
        return font;
    }

    @Override
    public void create() {

        // Font Setup
        font = new BitmapFont();
        font.setColor(Color.WHITE);

        spriteBatch = new SpriteBatch();
        this.setScreen(new MainMenuScreen(this));
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        font.dispose();
        spriteBatch.dispose();
    }
}
